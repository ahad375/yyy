# yyy

[![CI Status](http://img.shields.io/travis/ahad11/yyy.svg?style=flat)](https://travis-ci.org/ahad11/yyy)
[![Version](https://img.shields.io/cocoapods/v/yyy.svg?style=flat)](http://cocoapods.org/pods/yyy)
[![License](https://img.shields.io/cocoapods/l/yyy.svg?style=flat)](http://cocoapods.org/pods/yyy)
[![Platform](https://img.shields.io/cocoapods/p/yyy.svg?style=flat)](http://cocoapods.org/pods/yyy)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

yyy is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'yyy'
```

## Author

ahad11, aalarifi64@gmail.com

## License

yyy is available under the MIT license. See the LICENSE file for more info.
